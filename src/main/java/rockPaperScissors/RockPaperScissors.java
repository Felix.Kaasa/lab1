package rockPaperScissors;

import java.rmi.StubNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        boolean gameOver = false;
        Random rand = new Random();

        while (!gameOver){
            System.out.println("Let's play round " + roundCounter);
            String playerChoice = getInput();
            String aiChoice = getAiChoice(rand);
            int winner = updateScore(playerChoice, aiChoice);
            if (winner == -1){
                System.out.println("Human chose " + playerChoice + ", computer chose " + aiChoice + ". It's a tie!");
            }
            else if (winner == 0){
                System.out.println("Human chose " + playerChoice + ", computer chose " + aiChoice + ". Human wins!");
                humanScore++;
            }
            else{
                System.out.println("Human chose " + playerChoice + ", computer chose " + aiChoice + ". Computer wins!");
                computerScore++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            while (true){
                String replay = readInput("Do you wish to continue playing? (y/n)?");
                if (replay.equals("n")){
                    gameOver = true;
                    System.out.println("Bye bye :)");
                    break;
                }
                else if (replay.equals("y")){
                    break;
                }
                else{
                    System.out.println("I do not understand cardboard. Could you try again?");
                }
            }
            roundCounter++;
        }
    }

    public String getInput(){
        String output = "";
        output = readInput("Your choice (Rock/Paper/Scissors)?");
        output = output.toLowerCase();

        while (!rpsChoices.contains(output)){
            System.out.println("I do not understand cardboard. Could you try again?");
            output = readInput("Your choice (Rock/Paper/Scissors)?");
            output = output.toLowerCase();
        }
        return output;
    }

    public String getAiChoice(Random rand){
        String choice = rpsChoices.get(rand.nextInt(3));
        return choice;
    }

    public int updateScore(String choice1, String choice2){
        if (choice1.equals(choice2)){
            return -1;
        }
        else if (choice1.equals("rock") && choice2.equals("paper") || choice1.equals("paper") && choice2.equals("scissors") || choice1.equals("scissors") && choice2.equals("rock")){
            return 1;
        }
        else{
            return 0;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
